<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTest extends TestCase
{
    /**
     * @test
     */

    function test_if_return_200_visualizar_dados()
    {   
        $this->get('visualizar')
        ->assertStatus(200)
        ->assertSee('Furadeira X');

    }

    function test_delete_data()
    {
        $this->delete('excluir/5adef4f82607e92a1c002612')
        ->assertStatus(200);
    }

    function test_update_data()
    {
        $this->post('excluir/5adef4f82607e92a1c002614', ['name' => 'Furadeira TOP'])
        ->assertStatus(200);
    }
}
