<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Produtos extends Eloquent
{
    
    protected $collection = 'produtos';
    protected $fillable = ['lm', 'name', 'free_shipping', 'description', 'price'];
}
