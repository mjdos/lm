<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Response;
use App\Jobs\JobCadastro;
use App\Produtos;
use DB;

class ExcelController extends Controller
{

    public function visualizar(){ //Visualizar os produtos cadastrados
        
        $dados = Produtos::all();
        
        if($dados == '[]'){
            return Response::json(['response'=>'Nenhum registro encontrado'], 400);
        }else{
            return Response::json($dados, 200);
        }
    }

    public function cadastrar()
    {   
        $path = storage_path('products_teste_webdev_leroy.xlsx'); //Caminho do Excel
       
        $produtos = (new FastExcel)->import($path, function ($line) { //Lendo o Excel
            
            dispatch(new JobCadastro($line)); //Gerando a Queue

        });

        return ["status" => 'Produtos Cadastrados'];

    }

    public function atualizar($id, Request $request){

       $data = sizeof($_POST) > 0 ? $_POST : json_decode($request->getContent(), true); // Pega o post ou o raw

       Produtos::where('_id', $id)
       ->update([
                'name'          => $data['name'], 
                'free_shipping' => $data['free_shipping'], 
                'description'   => $data['description'],
                'price'         => $data['price']
                ]);

        return ["status" => 'Produtos Atualizados'];
    }

    public function excluir($id){
        
        Produtos::destroy($id);

        return ["status" => 'Produto Excluido'];

    }

}
