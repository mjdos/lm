<?php

namespace App\Jobs;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Produtos;

use DB; 

class JobCadastro implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $dados;

    public function __construct($dados)
    {
        $this->dados=$dados;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->dados;

        Produtos::create([
            'lm' => $data['lm'],
            'name' => $data['name'],
            'free_shipping' => $data['free_shipping'],
            'description' => $data['description'],
            'price' => $data['price']
        ]);

    }
}
